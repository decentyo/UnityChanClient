﻿using UnityEngine;

public class NetObject : MonoBehaviour
{
	[SerializeField] protected float repeatRate = 0.2f;
	public bool Owner { get; set; }

	[SerializeField]
	public string Id { get; set; }

	protected Vector3 targetPosition;
	private float speed = 8f;

	// Use this for initialization
	protected virtual void Start()
	{
		if (Owner)
		{
			InvokeRepeating("NetUpdate", 0, repeatRate);
		}
	}

	private void Update()
	{
		if (!Owner)
		{
			print("pos: " + targetPosition);
			float step = speed * Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);
		}
	}

	private void NetUpdate()
	{
		NetManager.Instance.Emit("updateObject", Id, transform.position);
	}

	//	Update by another user
	public void ClientUpdate(JSONObject data)
	{
		var posJson = data["position"];
		var x = 0f;
		var y = 0f;
		var z = 0f;

		posJson.GetField(ref x, "x");
		posJson.GetField(ref y, "y");
		posJson.GetField(ref z, "z");
		targetPosition = new Vector3(x, y, z);
	}
}