﻿#region License

/*
 * TestSocketIO.cs
 *
 * The MIT License
 *
 * Copyright (c) 2014 Fabio Panettieri
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#endregion

using System.Collections;
using System.Collections.Generic;
using SocketIO;
using UnityEngine;

public class NetManager : MonoBehaviour
{
	public delegate void CallbackFunc();

	public delegate void FireAction(Vector2 pos, Vector2 dir);

	public delegate void JoinedToRoomCallbackFunc(int game);

	[SerializeField] private Transform clientPrefab;
	private JoinedToRoomCallbackFunc joinedToRoomCallback;
	[SerializeField] private Transform mainCamera;
	private Dictionary<string, NetObject> netObjects = new Dictionary<string, NetObject>();
	[SerializeField] private Transform ownHeroPrefab;
	[SerializeField] private SocketIOComponent socket;
	private NetObject[] serverObjects;
	public static NetManager Instance { get; private set; }
	public static event FireAction onFired;

	public void Start()
	{
		Instance = this;

		socket.On("open", OnOpened);
		socket.On("error", OnError);
		socket.On("close", OnClosed);

		socket.On("connected", OnConnected);
		socket.On("newClient", OnNewClient);
		socket.On("updateObject", OnUpdateObject);
		socket.On("newServer", OnNewServer);
		socket.On("userDisconnected", OnUserDisconnected);

		InitNetObjects();
	}

	private void OnUserDisconnected(SocketIOEvent e)
	{
		Debug.Log("OnUserDisconnected");
		var id = "";
		e.data.GetField(ref id, "id");
		Object.Destroy(netObjects[id].gameObject);
	}

	private void InitNetObjects()
	{
		var i = 0;
		serverObjects = FindObjectsOfType<NetObject>();

		foreach (var obj in serverObjects)
		{
			var id = i.ToString();
			print(id);
			obj.Id = id;
			netObjects[id] = obj;
			i++;
		}
	}

	private void OnNewServer(SocketIOEvent e)
	{
		foreach (var obj in serverObjects)
		{
			obj.Owner = true;
		}
	}

	public void CreateRoom(int game, JoinedToRoomCallbackFunc func)
	{
		var jsonObject = new JSONObject();
		jsonObject.AddField("game", game);

		joinedToRoomCallback = func;
		socket.Emit("createRoom", jsonObject);

		socket.On("roomCreated", @event => { Debug.Log("Room was created"); });
	}

	private void OnOpened(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Open received: " + e.name + " " + e.data);
	}

	private void OnError(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Error received: " + e.name + " " + e.data);
	}

	private void OnClosed(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Close received: " + e.name + " " + e.data);
	}

	public void Emit(string message)
	{
		socket.Emit(message);
	}

	public void Emit(string message, JSONObject data)
	{
		socket.Emit(message, data);
	}

	public void Emit(string message, string id, Vector3 pos)
	{
		var jsonObject = new JSONObject();
		var posJson = new JSONObject();

		posJson.AddField("x", pos.x);
		posJson.AddField("y", pos.y);
		posJson.AddField("z", pos.z);

		jsonObject.AddField("id", id);
		jsonObject.AddField("position", posJson);

		Emit(message, jsonObject);
	}

	private void OnConnected(SocketIOEvent e)
	{
		Debug.Log("OnConnected");
		var id = "";
		e.data.GetField(ref id, "id");
		var obj = (Transform) Instantiate(ownHeroPrefab, new Vector3(0f, 0f, 0f), Quaternion.identity);
		var netObject = obj.GetComponent<NetObject>();
		netObject.Id = id;
		netObject.Owner = true;
		mainCamera.gameObject.SetActive(true);
	}

	private void OnNewClient(SocketIOEvent e)
	{
		var id = "";
		e.data.GetField(ref id, "id");
		Debug.Log("OnNewClient. Id: " + id);
		var obj = (Transform) Instantiate(clientPrefab, new Vector3(0f, 0f, 0f), Quaternion.identity);
		var netObject = obj.GetComponent<NetObject>();
		netObject.Id = id;
		netObjects.Add(id, netObject);
	}

	private void OnUpdateObject(SocketIOEvent e)
	{
		var id = "";
		e.data.GetField(ref id, "id");
		netObjects[id].ClientUpdate(e.data);
	}
}