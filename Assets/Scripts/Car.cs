﻿using UnityEngine;

public class Car : NetObject
{
	public float forwardSpeed = 7.0f;
	public float backwardSpeed = 2.0f;
	public float rotateSpeed = 2.0f;
	private Rigidbody rb;
	[SerializeField] private bool isFixedUpdate;
	private Vector3 velocity;
	private GameObject cameraObject;
	// Use this for initialization
	protected override void Start()
	{
		base.Start();
		rb = GetComponent<Rigidbody>();
		cameraObject = GameObject.FindWithTag("MainCamera");
		isFixedUpdate = true;
	}

	private void FixedUpdate()
	{
		if (!isFixedUpdate)
		{
			return;
		}

		var h = Input.GetAxis("Horizontal");
		var v = Input.GetAxis("Vertical");
		rb.useGravity = true;

		velocity = new Vector3(0, 0, v);
		velocity = transform.TransformDirection(velocity);
		if (v > 0.1)
		{
			velocity *= forwardSpeed;
		}
		else if (v < -0.1)
		{
			velocity *= backwardSpeed;
		}

		transform.localPosition += velocity*Time.fixedDeltaTime;
		transform.Rotate(0, h*rotateSpeed, 0);
		cameraObject.SendMessage("setCameraPositionJumpView");
	}
}